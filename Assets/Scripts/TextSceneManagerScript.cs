﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextSceneManagerScript : MonoBehaviour {

	public Text closeText;
	public Text farText;
	public Text closeWhiteText;
	public Text farWhiteText;

	public GameObject menu;
	public GameObject closeQuad;
	public GameObject farQuad;
	public GameObject closeBlackQuad;
	public GameObject farBlackQuad;

	void Start()
	{
		ControllerTracker.onTrackpadButtonPressed += advanceOne;
	}

	void advanceOne()
	{
		farQuad.SetActive(true);
		ControllerTracker.onTrackpadButtonPressed += advanceTwo;
		ControllerTracker.onTrackpadButtonPressed -= advanceOne;
	}
	void advanceTwo()
	{
		closeText.enabled = true;
		farText.enabled = false;
		farQuad.SetActive(false);
		ControllerTracker.onTrackpadButtonPressed += advanceThree;
		ControllerTracker.onTrackpadButtonPressed -= advanceTwo;
	}

	void advanceThree()
	{
		closeQuad.SetActive(true);
		ControllerTracker.onTrackpadButtonPressed += advanceFour;
		ControllerTracker.onTrackpadButtonPressed -= advanceThree;
	}

	void advanceFour()
	{
		closeText.enabled = false;
		closeQuad.SetActive(false);
		menu.SetActive(true);
		ControllerTracker.onTrackpadButtonPressed += advanceFive;
		ControllerTracker.onTrackpadButtonPressed -= advanceFour;
	}

	void advanceFive()
	{
		menu.SetActive(false);
		farWhiteText.enabled = true;
		farBlackQuad.SetActive(true);
		ControllerTracker.onTrackpadButtonPressed += advanceSix;
		ControllerTracker.onTrackpadButtonPressed -= advanceFive;
	}

	void advanceSix()
	{
		farWhiteText.enabled = false;
		farBlackQuad.SetActive(false);
		closeWhiteText.enabled = true;
		closeBlackQuad.SetActive(true);
		ControllerTracker.onTrackpadButtonPressed += advanceSeven;
		ControllerTracker.onTrackpadButtonPressed -= advanceSix;
	}

	void advanceSeven()
	{
		closeWhiteText.enabled = false;
		closeBlackQuad.SetActive(false);
		farText.enabled = true;
		ControllerTracker.onTrackpadButtonPressed += advanceOne;
		ControllerTracker.onTrackpadButtonPressed -= advanceSeven;
	}

}
