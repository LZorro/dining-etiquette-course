﻿using UnityEngine;
using System.Collections;
/*
public interface IVRController {
    Vector3 Position { get; }
    Vector3 Orientation { get; }
}

public class WebVRControllerWrapper : IVRController {

}
*/

/*
    This is the class that is the analog to StereoCamera, but for the controller instead of the camera.
    Similar to StereoCamera, it has a bunch of getValue functions that the webpage calls in javascript using
    SendMessage in order to push the controller's postion, rotation, angular and linear velocities, and
    the controller's button states back into unity. 
    Like StereoCamera, this object is a singleton. The singleton code is handeled in StereoCamera, though,
    because StereoCamera and ControllerTracker should always be children of the same ultimate parent.
    In addition, this class has public static events for when each of the buttons are pressed. That way,
    other classes can subscribe functions that they want executed when a button is pressed and unsubscribe
    from the event when they don't want to react to the input anymore, all without needing a reference
    to this class.
    -Logan 9/1/16

*/

public class ControllerTracker : MonoBehaviour {
    //instance of StereoCamera.cs so that the quaternion stuff can all be called from the 
    //same function rather than duplicates
    public StereoCamera stereoCamera;


	//float values representing how much a button is pressed (0 to 1) where 1 is pressed
    public float trackpadButtonValue;
    public float triggerButtonValue;    //This value actually hovers around .01 to .007 after one press and release, not full 0
    public float gripButtonValue;
    public float menuButtonValue;

    public string id;       // determines name of gamepad being used

    //the threshold for when the trigger should no longer be counted as being pressed, since it doesn't always go back to 0
    public float triggerThreshold;

    Vector3 controllerEuler = new Vector3();
    Vector3 controllerPosition = new Vector3();
    public Vector3 controllerVelocity = new Vector3();
    public Vector3 controllerAngularVelocity = new Vector3();

    //delgate types and the associated events. The delegate signature determines what kind
    //of signature the functions that get subscribed to the events can have. In this case,
    //only functions with no parameters and no return value can be subscribed.
    public delegate void triggerPressed();
    public static event triggerPressed onTriggerPressed;

    public delegate void trackpadButtonPressed();
    public static event trackpadButtonPressed onTrackpadButtonPressed;
    bool trackpadHasReset;

    public delegate void gripButtonPressed();
    public static event gripButtonPressed onGripButtonPressed;
    bool gripButtonHasReset;

    public delegate void menuButtonPressed();
    public static event menuButtonPressed onMenuButtonPressed;
    bool menuButtonHasReset;

	void Update () {
		var controllerUnityEuler = stereoCamera.ConvertWebVREulerToUnity(controllerEuler);
        controllerUnityEuler.x = -controllerUnityEuler.x;
        controllerUnityEuler.z = -controllerUnityEuler.z;
        this.transform.rotation = Quaternion.Euler(controllerUnityEuler);

        controllerPosition.z *= -1;
        this.transform.position = controllerPosition;

        //keeping track of whether the buttons have been released since their last press. Otherwise,
        //the events get called multiple times on every press
        if(trackpadButtonValue == 0) trackpadHasReset = true;
        if(gripButtonValue == 0) gripButtonHasReset = true;
        if(menuButtonValue == 0) menuButtonHasReset = true;

        //call the events for a button if it's pressed. Calling the event calls every function
        //subscribed to it at that time.
        if(triggerButtonValue > triggerThreshold && onTriggerPressed != null)
        {
            onTriggerPressed();
        }
        if(trackpadButtonValue > 0 && trackpadHasReset && onTrackpadButtonPressed != null)
        { 
            onTrackpadButtonPressed();
            trackpadHasReset = false;
        }
        if(gripButtonValue > 0 && gripButtonHasReset && onGripButtonPressed != null)
        {
            onGripButtonPressed();
            gripButtonHasReset = false;
        }
        if(menuButtonValue > 0 && menuButtonHasReset && onMenuButtonPressed != null) 
        {
            onMenuButtonPressed();
            menuButtonHasReset = false;
        }
	}



    //functions that get called by the javascript in the webpage and send the controller's
    //position-rotation-velocity info into unity
//
    void getControllerX(float val)
    {
    	controllerPosition.x = val;
    }
    void getControllerY(float val)
    {
    	controllerPosition.y = val;
    }
    void getControllerZ(float val)
    {
    	controllerPosition.z = val;
    }
//
    void getControllerVelocityX(float val)
    {
        controllerVelocity.x = val;
    }
    void getControllerVelocityY(float val)
    {
        controllerVelocity.y = val;
    }
    void getControllerVelocityZ(float val)
    {
        controllerVelocity.z = -val;
    }
//
    void getControllerAngularVelocityX(float val)
    {
        controllerAngularVelocity.x = val;
    }
    void getControllerAngularVelocityY(float val)
    {
        controllerAngularVelocity.y = val;
    }
    void getControllerAngularVelocityZ(float val)
    {
        controllerAngularVelocity.z = val;
    }
//
    void controller_euler_x(float val)
    {
    	controllerEuler.x = val;
    }
    void controller_euler_y(float val)
    {
    	controllerEuler.y = val;
    }
    void controller_euler_z(float val)
    {
    	controllerEuler.z = val;
    }
//
    void getTrackpadButtonState(float pressedVal)
    {
        trackpadButtonValue = pressedVal;
    }
    void getGripButtonState(float pressedVal)
    {
        gripButtonValue = pressedVal;
    }
    void getTriggerState(float pressedVal)
    {
        triggerButtonValue = pressedVal;
    }
    void getMenuButtonState(float pressedVal)
    {
        menuButtonValue = pressedVal;
    }
    void getControllerID(string name)
    {
        id = name;
    }
//
}
