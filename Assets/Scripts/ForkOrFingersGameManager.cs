﻿using UnityEngine;
using System.Collections;
// using System;
using System.Linq;
using UnityEngine.UI;
using System.Collections.Generic;

/*
	Class that controls the flow of the Fork Or Fingers scene.
	In charge of managing the text in the scene, spawning the food, keeping track of foods
	the player eats correctly or incorrectly.
	-Logan 9/1/16

*/



public class ForkOrFingersGameManager : MonoBehaviour {

	private Vector3 spawnOffset = new Vector3(0.1f, 0.1f, 0.025f);

	public Text welcomeText;
	public Text lessonPortionIntroductionText;
	public Text testIntroductionText;
	public Text completionText;
	public Text feedbackTextPrimary;
	public Text feedbackTextSecondary;
	public Text foodSpawningText;
	public Text correctionFoodList;
	public Text correctionUtensilList;
	public GameObject generalTextBackgroundQuad;
	public GameObject feedbackTextBackgroundQuad;
	public GameObject completionTextBackgroundQuad;

	private GameObject[] plates;
	private GameObject[] food;
	private GameObject[] emptyParents;	//list of empty prefab objects to remove after their food children are eaten

	public GameObject bell;
	public GameObject bellInstructions;
	public GameObject controllerInstructions;
	public GameObject menu;


	[SerializeField]
	//List of all food+namecard prefab references that gets populated in the editor
	private List<GameObject> foodList;	
	//dictionary of <foodname, food reference> so that foods can be spawned over and over just by knowing the food's name
	private Dictionary<string, GameObject> foodDictionary;
	//used differently in the lesson portion vs in the test portion
	//in lesson portion, it's the food the player has not yet eaten correctly, so keep spawning that food until they do
	//in test portion, it's the list of food that hasn't yet been presented to the player
	private List<string> remainingFoods;
	//list to keep track of foods the player ate incorrectly last round to try to make sure they're not shown that food
	//in the following round if it can be helped
	private List<string> lastRoundFailedFood;
	//list to keep track of all the foods the player ate incorrectly in the test portion
	private List<string> testFailedFoods;

	private bool allDone;
	private bool inLesson;
	private bool inTest;

	//bool to indicate when the coroutine that causes a pause between rounds is running so that the 
	//game doesn't start the coroutine more than one time
	private bool foodBeingLoaded;	

	void Start () {
		plates = GameObject.FindGameObjectsWithTag("Plate");
		findFoodsAndParents();
		inLesson = false;	//if this is ever set to true, it's for debug purposes
		inTest = false;
		allDone = false;
		foodBeingLoaded = false;
		bell.SetActive(false);
		bellInstructions.SetActive(false);
		controllerInstructions.SetActive(false);
		populateDictionary();
		remainingFoods = new List<string>(foodDictionary.Keys);
		randomizeList(remainingFoods);
		lastRoundFailedFood = new List<string>();
		testFailedFoods = new List<string>();

		//turn off all the texts/quads except for the ones I want left on so that if I edit in the editor and leave some activated it won't matter
		welcomeText.enabled = true;
		lessonPortionIntroductionText.enabled = false;
		testIntroductionText.enabled = false;
		completionText.enabled = false;
		feedbackTextPrimary.enabled = false;
		feedbackTextSecondary.enabled = false;
		foodSpawningText.enabled = false;
		correctionFoodList.enabled = false;
		correctionUtensilList.enabled = false;
		generalTextBackgroundQuad.SetActive(false);
		feedbackTextBackgroundQuad.SetActive(false);
		completionTextBackgroundQuad.SetActive(false);
		feedbackTextPrimary.text = "";
		feedbackTextSecondary.text = "";
		foodSpawningText.text = "";
		correctionFoodList.text = "";
		correctionUtensilList.text = "";

		ControllerTracker.onTrackpadButtonPressed += prepareForLesson;
	}

	void Update () {

		if(!(inLesson || inTest)) return;

		if(allDone)
		{
			return;
		}

		bool foodRemains = false;
		for(int i = 0; i < food.Length; i++)
		{
			if(food[i]) 
			{
				foodRemains = true;
				break;
			}
		}
		if(!foodRemains)
		{
			if(inLesson && !foodBeingLoaded)
			{
				foodBeingLoaded = true;
				StartCoroutine(waitThenSpawnFoodForLesson(3));
			}
			if(inTest) 
			{
				deleteEmptyParents();
				spawnFoodForTest();
			}
		}
	
	}

	//function called when lesson portion ends to prepare the scene and player for the test
	void prepareForTest()
	{
		inLesson = false;
		testIntroductionText.enabled = true;
		generalTextBackgroundQuad.SetActive(false);
		feedbackTextPrimary.enabled = false;
		feedbackTextSecondary.enabled = false;
		foodSpawningText.enabled = false;
		feedbackTextBackgroundQuad.SetActive(false);

		//re-populate the remainingFoods list
		remainingFoods = new List<string>(foodDictionary.Keys);
		randomizeList(remainingFoods);

		ControllerTracker.onTrackpadButtonPressed += beginTest;
	}

	//function to begin the test after prepareForTest has been called
	void beginTest()
	{
		inTest = true;
		testIntroductionText.enabled = false;
		generalTextBackgroundQuad.SetActive(false);
		ControllerTracker.onTrackpadButtonPressed -= beginTest;
	}

	//function to be called at the end of the test to display the results
	void finishTest()
	{
		//Call the javascript function "finishCourse" which tells the LMS the user completed the course
		Application.ExternalCall("finishCourse");

		//initial position and scale that the quad should have for the resizing to work correctly
		//quad gets resized to fit however many foods the player ate incorrectly
		completionTextBackgroundQuad.transform.localScale = new Vector3(1.768184f, 0.4126371f, 1);
		completionTextBackgroundQuad.transform.position = new Vector3(-0.697f, 1.73f, 0.775f);

		allDone = true;
		completionText.enabled = true;
		correctionFoodList.enabled = true;
		correctionUtensilList.enabled = true;
		completionTextBackgroundQuad.SetActive(false);
		

		bell.SetActive(false);
		bellInstructions.SetActive(false);
		controllerInstructions.SetActive(false);
		//menu.transform.position = new Vector3(-0.3f, 0.6f, -0.32f);
		//menu.transform.rotation = Quaternion.Euler(15, 33, 0);
		menu.SetActive(true);

		int numberEatenCorrectly = foodList.Count - testFailedFoods.Count;
		float score = (float)numberEatenCorrectly / (float)foodList.Count;
		if(score >= 0.9f)
		{
			completionText.text = "Excellent Job!" + System.Environment.NewLine + "You ate <color=#00ffffff>" + numberEatenCorrectly + "</color> out of <color=#00ff00ff>" + foodList.Count + "</color> dishes correctly." + System.Environment.NewLine + " You are sure to land a lucrative contract and earn that promotion!" + System.Environment.NewLine;
		}else if(score >= 0.7f)
		{
			completionText.text = "Well done!" + System.Environment.NewLine + "You ate <color=#00ffffff>" + numberEatenCorrectly + "</color> out of <color=#00ff00ff>" + foodList.Count + "</color> dishes correctly." + System.Environment.NewLine + " You are well respected among your peers and will likely get a favorable review from your manager." + System.Environment.NewLine;
		}else if(score >=0.5f)
		{
			completionText.text = "Nicely done, but you have room for improvement." + System.Environment.NewLine + "You ate <color=#00ffffff>" + numberEatenCorrectly + "</color> out of <color=#00ff00ff>" + foodList.Count + "</color> dishes correctly." + System.Environment.NewLine + " Your coworkers give you a hard time about your uncouth eating habits." + System.Environment.NewLine;
		}else{
			completionText.text = "You have failed this assessment." + System.Environment.NewLine + "You ate <color=#00ffffff>" + numberEatenCorrectly + "</color> out of <color=#00ff00ff>" + foodList.Count + "</color> dishes correctly." + System.Environment.NewLine + " Your business partner declines your latest offer, and you are unlikely to be invited to lunch again." + System.Environment.NewLine;
		}

		/*
			(old text as of 10/14/16 - TJV)
			score > 90
			Excellent Job, Traveler!
			You ate Z out of X dishes correctly. You will be able to seamlessly blend in to human society. Nobody will notice your extraterrestial origins. Just, please
			remember our agreement... our cows are valuable to us.

			90 > score > 70
			Well done, Traveler!
			You ate Z out of X dishes correctly. You should now be able to avoid the most common dining faux pas and blend in to all but the most refined groups of humans.
			Please just remember our agreement and leave our cows alone.

			70 > score > 50
			Nicely done, but you have room for improvement.
			You ate Z out of X dishes correctly. At your current level of dining etiquette, it's likely that people will be suspicious of your origins. Your safest bet
			will probably be to claim to be a time traveling Neanderthal. That's what my son does. (He's 7)

			50 > score
			Hmm... Perhaps you're better off trying to blend into a different planet's population.
			You ate Z out of X dishes correctly. On the bright side, with dining etiquette skills like that, people are unlikely to notice your extra eyes and antenna I guess.
		*/

		//only add what they ate wrong if they actually ate anything wrong
		if(testFailedFoods.Count > 0)
		{
			//intial: pos : 1.73	scale: 0.412637
			completionTextBackgroundQuad.transform.localScale += new Vector3(0, 0.078f, 0) * testFailedFoods.Count + new Vector3(0, 0.2455f, 0);
			completionTextBackgroundQuad.transform.position += new Vector3(0, -0.039f, 0) * testFailedFoods.Count + new Vector3(0, 0.015f, 0);


			completionText.text += System.Environment.NewLine + "You used the wrong utensil when eating the following dishes: " + System.Environment.NewLine;
			correctionFoodList.text = "Foods: " + System.Environment.NewLine;
			correctionUtensilList.text = "Correct Utensil: " + System.Environment.NewLine;
			foreach(string food in testFailedFoods)
			{
				correctionFoodList.text += food + System.Environment.NewLine;
				if(foodDictionary[food].transform.GetChild(0).tag.Contains("Fork")) correctionUtensilList.text += "Fork" + System.Environment.NewLine;
				else correctionUtensilList.text += "Fingers" + System.Environment.NewLine;
			}
		}
	}

	//coroutine to spawn food in the lesson on after a brief pause
	IEnumerator waitThenSpawnFoodForLesson(int seconds)
	{
		while(seconds > 0)
		{
			foodSpawningText.text = "More food coming in: " + seconds;
			yield return new WaitForSeconds(1);
			seconds--;
		}
		foodSpawningText.text = "";
		deleteEmptyParents();
		spawnFoodForLesson();
	}

	//function that spawns the food during the test portion. Simply spawns food off of the remaingingFoods
	//list and then removes the food from that list, since in the test portion the player only gets one chance
	//to eat each food.
	void spawnFoodForTest()
	{
		//if they've correctly eaten all of the foods, we're all done, so return
		if(remainingFoods.Count == 0)
		{
			finishTest();
			return;
		}

		//Spawn only the smallest number between number of available plates and number of food left to spawn
		int min = plates.Length;
		if(remainingFoods.Count < min) min = remainingFoods.Count;

		for(int i = 0; i < min; i++)
		{
			Instantiate(foodDictionary[remainingFoods[0]], plates[i].transform.position + spawnOffset, Quaternion.identity);
			remainingFoods.RemoveAt(0);
		}

		findFoodsAndParents();
	}

	//function to spawn food for the lesson portion. It tries not to spawn food that the player ate incorrectly
	//last round so that the player doesn't see the same food in consecutive rounds.
	void spawnFoodForLesson()
	{
		//if they've correctly eaten all of the foods, we're all done, so return
		if(remainingFoods.Count == 0)
		{
			prepareForTest();
			return;
		}

		//Spawn only the smallest number between number of available plates and number of food left to spawn
		int min = plates.Length;
		if(remainingFoods.Count < min) min = remainingFoods.Count;

		//Loop through the remaining foods and spawn them. Try to spawn ones they didn't mess up on the round before first.
		int foodsSpawned = 0;	//keep track of how many new foods (not ones from last round) get spawned
		for(int i = 0; i < remainingFoods.Count; i++)	
		{
			if(foodsSpawned >= min) break; //only spawn as many foods as there are plates or foods if less foods remain than there are plates
			if(!lastRoundFailedFood.Contains(remainingFoods[i]))	//first, try to spawn foods they didn't just try to eat last round
			{
				Instantiate(foodDictionary[remainingFoods[i]], plates[foodsSpawned].transform.position + spawnOffset, Quaternion.identity);
				foodsSpawned++;
			}
		}

		//now, if foodsSpawned < min, that means that not all of the plates were filled because the food that would spawn there is on the failedList
		//so, fill the remaining plates with foods from the failedList
		if(foodsSpawned < min)
		{
			for(int i = 0; i < (min - foodsSpawned); i++)
			{
				Instantiate(foodDictionary[lastRoundFailedFood[i]], plates[i + foodsSpawned].transform.position + spawnOffset, Quaternion.identity);
			}
		}

		lastRoundFailedFood.Clear();	//clear the list of foods they failed at last round in preparation for a new round
		findFoodsAndParents();
		foodBeingLoaded = false;
	}

	//the first function that gets called to advance the experience in this scene
	void prepareForLesson()
	{
		welcomeText.enabled = false;
		lessonPortionIntroductionText.enabled = true;
		ControllerTracker.onTrackpadButtonPressed += beginLesson;
		ControllerTracker.onTrackpadButtonPressed -= prepareForLesson;
	}

	//the second function that gets called to advance the experience in this scene
	void beginLesson()
	{
		bell.SetActive(true);
		bellInstructions.SetActive(true);
		controllerInstructions.SetActive(true);
		lessonPortionIntroductionText.enabled = false;
		generalTextBackgroundQuad.SetActive(false);
		feedbackTextPrimary.enabled = true;
		feedbackTextSecondary.enabled = true;
		foodSpawningText.enabled = true;
		feedbackTextBackgroundQuad.SetActive(false);
		inLesson = true;
		spawnFoodForLesson();
		ControllerTracker.onTrackpadButtonPressed -= beginLesson;
	}

	//Function to fill a dictionary with <name, object reference> pairs for each of the foods
	//using the foodList that gets populated in the inspector view as a template
	void populateDictionary()
	{
		foodDictionary = new Dictionary<string, GameObject>();
		foreach(GameObject a in foodList)
		{
			foodDictionary.Add(a.name, a);
		}
	}

	//called when food gets eaten correctly, it takes it off of the remaining foods list.
	public void foodEatenCorrectly(GameObject food)
	{
		if(inLesson) 
		{
			remainingFoods.Remove(food.name);
			feedbackTextPrimary.text = "Correct!";
			addUtensilFeedback(food.name);
			food.GetComponent<FoodBehavior>().checkMark.SetActive(true);

		}
	}

	//called when food gets eaten incorrectly, it stores the food name to keep track
	//and it LEAVES the food on the remainingFoodsList
	public void foodEatenIncorrectly(GameObject food)
	{
		if(inLesson) 
		{
			lastRoundFailedFood.Add(food.name);
			feedbackTextPrimary.text = "Incorrect";
			addUtensilFeedback(food.name);
			food.GetComponent<FoodBehavior>().xSymbol.SetActive(true);
			// StartCoroutine(wrongAnswerVibration());

		}
		if(inTest && !testFailedFoods.Contains(food.name)) testFailedFoods.Add(food.name);
	}

	//function to make the controller vibrate. Calls to it are commented out because vibration can cause webGL
	//to lose context when using the current (as of 8/29/16) build of chromium
	IEnumerator wrongAnswerVibration()
	{
		Application.ExternalCall("vibrateController", 40);
		yield return new WaitForSeconds(0.2f);
		Application.ExternalCall("vibrateController", 100);
	}

	//function to tell the user specifically what utensil they are supposed to use when eating a food.
	//This text pops up in front of them about a certain food right after they ate that specific food,
	//in combination with "Correct!" or "Incorrect"
	void addUtensilFeedback(string foodName)
	{
		string correctUtensil;
		if(foodDictionary[foodName].transform.GetChild(0).tag.Contains("Fork")) correctUtensil = "Fork";
		else correctUtensil = "Fingers";
		feedbackTextSecondary.text = foodName + " - Correct: " + correctUtensil;
	}

	//Randomizes the order of elements in a List<string>
	void randomizeList(List<string> myList)
	{
		for(int i = 0; i < myList.Count - 1; i++)
		{
			int randomIndex = Random.Range(i, myList.Count);
			string temp = myList[i];
			myList[i] = myList[randomIndex];
			myList[randomIndex] = temp;
		}
	}

	//Finds all of the food and empty parent objects in the scene to keep track of eating progress
	//and to clean up later when we don't want them in the scene anymore
	void findFoodsAndParents()
	{
		GameObject[] fingerFoods = GameObject.FindGameObjectsWithTag("FingerFood");
		GameObject[] forkFoods = GameObject.FindGameObjectsWithTag("ForkFood");
		food = fingerFoods.Concat(forkFoods).ToArray();
		emptyParents = GameObject.FindGameObjectsWithTag("Prefab Empty Parent");
	}

	//deletes the empty parent objects (and the child Name Card) of food that has been eaten
	void deleteEmptyParents()
	{
		for(int i = 0; i < emptyParents.Length; i++)
		{
			Destroy(emptyParents[i]);
		}
	}

	//unsubscribe all of the functions in here that might be subscribed to one of the button events
	//in case the player ever leaves the scene mid-way through the activity
	void OnDestroy()
	{
		ControllerTracker.onTrackpadButtonPressed -= prepareForLesson;
		ControllerTracker.onTrackpadButtonPressed -= beginLesson;
		ControllerTracker.onTrackpadButtonPressed -= beginTest;
		BellBehavior.onBellRung -= spawnFoodForLesson;
		BellBehavior.onBellRung -= spawnFoodForTest;
		BellBehavior.onBellRung -= deleteEmptyParents;
	}

}
