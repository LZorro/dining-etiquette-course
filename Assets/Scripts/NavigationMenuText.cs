﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class NavigationMenuText : MonoBehaviour {

	/*
		Generic script attached to the 3D text objects on each of the Menus that are used for 
		navigation between scenes. Set the scene you want a certain text to navigate to when
		clicked on by filling in the name of that scene in the public "sceneToLoad" field in
		the editor.
		This script also keeps track of whether any other pieces of text that are on the same
		menu are being touched already so that if you touch two pieces of text at once, it'll only
		highlight the first text you touched and will only load the scene of the first text touched.
		-Logan 9/15/16

	*/
	
	public GameObject border;
	public string sceneToLoad;
	public bool beingTouched;
	public NavigationMenuText otherText;
	private bool subscribed;

	void Start(){
		subscribed = false;
		beingTouched = false;
	}

	//when the controller touches the menu text, highlight it with the border
	//and subscribe the loadMenuScene function to the triggerPress event
	void OnTriggerEnter(Collider other)
	{
		if((otherText && otherText.beingTouched) || !other.attachedRigidbody) return;
		if(other.attachedRigidbody.tag == "GameController" && !subscribed)
		{
			beingTouched = true;
			subscribed = true;
			border.SetActive(true);
			ControllerTracker.onTriggerPressed += loadScene;
		}	
	}

	//when the controller stops touching the text, turn of the highlight
	//and unsubscribe the loadMenu function from the triggerPress event
	void OnTriggerExit(Collider other)
	{
		if(other.attachedRigidbody.tag == "GameController")
		{
			beingTouched = false;
			subscribed = false;
			border.SetActive(false);
			ControllerTracker.onTriggerPressed -= loadScene;
		}
	}

	//when loadMenuScene gets called, the object is destroyed before OnTriggerExit is called,
	//so the loadMenu function has to be unsubscribed in OnDestroy as well
	void OnDestroy()
	{
		ControllerTracker.onTriggerPressed -= loadScene;
	}

	void loadScene()
	{
		SceneManager.LoadScene(sceneToLoad);
	}
}
