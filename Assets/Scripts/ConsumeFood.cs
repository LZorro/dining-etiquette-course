﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ConsumeFood : MonoBehaviour {

	/*
		Scipt that's attached to a trigger collider just underneath the headset, roughly where a player's mouth is.
		This script destroys food that comes into contact with it and calls the FoF scene GameManager's foodEatenCorrectly
		or foodEatenIncorrectly depending on what state the controller was in (fork or fingers) and on what type of food was 
		destroyed (ForkFood or FingerFood).

		TODO: Currently, the player will eat food whenever it touches the collider, not just food that the player brought 
		towards their face with the controller. As in, they can just lean down and eat the food if they wanted to. Make it 
		so that only food that is currently being held by the player can be eaten.

		-Logan 9/1/16
	*/


	private HandForkBehavior controllerScript;


	private ForkOrFingersGameManager gameManager;

	//values for the strength and behavior of the explosion when the wrong utensil is used to eat food (old idea for negative feedback)
	// public float force;
	// public float radius;
	// public float popFactor;		//basically how much the explosion pops things up into the air vs just blowing them straight away

	void Start()
	{
		findGameManager();
		controllerScript = GameObject.Find("FirstController").GetComponent<HandForkBehavior>();
		//add a delegate to the sceneLoaded event to get called when a new scene is loaded
		SceneManager.sceneLoaded += onSceneLoad;
	}

	void OnDestroy()
	{
		//Unsubscribe from the scene loaded event if this object ever gets destroyed
		SceneManager.sceneLoaded -=onSceneLoad;
	}

	void OnTriggerEnter(Collider other)
	{
		GameObject food = other.attachedRigidbody.gameObject;
		eatFood(food);
	}

	public void eatFood(GameObject food)
	{
		//case where the user uses the correct utensil to eat the food
		if(food.CompareTag("FingerFood") && !controllerScript.isFork ||
				food.CompareTag("ForkFood") && controllerScript.isFork)
		{
			
			// FoodBehavior otherScript = other.GetComponent<FoodBehavior>();
			// AudioSource.PlayClipAtPoint(otherScript.myDestroySound, other.transform.position, Random.Range(otherScript.minVol, otherScript.maxVol));
			gameManager.foodEatenCorrectly(food);
			food.GetComponent<FoodBehavior>().clearSelection();
			Destroy(food);
		}
		//case where the user uses the incorrect utensil
		if(food.CompareTag("FingerFood") && controllerScript.isFork ||
				food.CompareTag("ForkFood") && !controllerScript.isFork)
		{
			
			gameManager.foodEatenIncorrectly(food);
			food.GetComponent<FoodBehavior>().clearSelection();
			Destroy(food);
			//get a list of colliders within "radius" and then add an explosion force to all of them
			// Collider[] colliders = Physics.OverlapSphere(other.GetComponent<Transform>().position, radius);
			// foreach(Collider c in colliders)
			// {
			// 	if(c.GetComponent<Rigidbody>() == null) continue;	//only move things with rigid bodies
			// 	c.GetComponent<Rigidbody>().AddExplosionForce(force, other.GetComponent<Transform>().position, radius, popFactor, ForceMode.Impulse);
			// }
		}
	}

	void findGameManager()
	{
		GameObject gameManagerObject = GameObject.FindWithTag("GameManager");
		if(gameManagerObject != null){
			gameManager = gameManagerObject.GetComponent<ForkOrFingersGameManager>();
		}
		// if(gameManager == null) Debug.Log("Cannot find 'GameManager' script from within ConsumeFood script");
	}

	void onSceneLoad(UnityEngine.SceneManagement.Scene scene, UnityEngine.SceneManagement.LoadSceneMode loadMode)
	{
		if(scene.name == "Fork or Fingers") findGameManager();
	}

}
