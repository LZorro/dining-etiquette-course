﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuSceneGameManager : MonoBehaviour {

	/*
		Script to manage the flow of the Menu scene. Has a static bool to keep track of whether
		it's the first time the scene has been loaded or not so that the welcome text is only displayed
		once.
		-Logan 9/1/16
	*/

	// static bool haveLoadedBefore;

	public GameObject menu;
	public GameObject gazer;
	public Text firstText;
	public Text secondText;
	public Text firstInstructions;
	public Text secondInstructions;


	void Start()
	{
		// if(!haveLoadedBefore)
		// {
		// 	ControllerTracker.onTrackpadButtonPressed += advanceInstructions;
		// 	haveLoadedBefore = true;
		// }else{
		// 	advanceInstructions();
		// }

		ControllerTracker.onTrackpadButtonPressed += advanceInstructions;
	}

	public void advanceInstructions()
	{
		if (GameObject.Find("FirstController").GetComponent<ControllerTracker>().id == "Oculus Remote")
		{
			if (gazer) {
				gazer.SetActive(true);
			}
		}

		firstText.enabled = false;
		firstInstructions.enabled = false;
		secondText.enabled = true;
		secondInstructions.enabled = true;
		menu.SetActive(true);
		ControllerTracker.onTrackpadButtonPressed -= advanceInstructions;
	}

	void OnDestroy()
	{
		ControllerTracker.onTrackpadButtonPressed -= advanceInstructions;
	}
}
