﻿using UnityEngine;
using System.Collections;

public class DestroyByBoundary : MonoBehaviour {

	/*
		This scipt is attached to a collider that surrounds the table and play area in the FoF scene. 
		It destroys the food the player throws/drops and calls the gameManager's foodEatenIncorreclty on it.
	*/

	public ForkOrFingersGameManager gameManager;

	void OnTriggerExit(Collider other)
	{
		if(other.attachedRigidbody.tag.Contains("Food"))
		{
			gameManager.foodEatenIncorrectly(other.attachedRigidbody.gameObject);
			Destroy(other.attachedRigidbody.gameObject);
		}

	}
}
