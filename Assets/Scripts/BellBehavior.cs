﻿using UnityEngine;
using System.Collections;

public class BellBehavior : MonoBehaviour {

	/*
		Script that hosts the event for when the bell gets rung. Bell gets rung whenever the player
		hits it and then the onBellRung event gets triggered.
		-Logan 9/1/16
	*/

	public delegate void bellRung();
	public static event bellRung onBellRung;
	
	void OnTriggerEnter(Collider other)
	{
		if(other.attachedRigidbody.tag == "GameController" && onBellRung != null) 
		{
			onBellRung();
		}
	}
}
