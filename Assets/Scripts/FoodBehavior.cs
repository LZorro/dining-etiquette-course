﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FoodBehavior : MonoBehaviour {

	// public AudioClip myDestroySound;		//for when sound works. This could be specific to each food, like crunching or slurping or something.
	//references to the visual feedback symbols that're a part of the food+namecard prefabs
	public GameObject xSymbol;
	public GameObject checkMark;
	public GameObject highlight_finger;
	public GameObject highlight_fork;
	public bool isFork;
	// public float minVol;	//for when sound actually works, playing sound at random volume between min and max sounds more natural than exactly
	// public float maxVol;	//the same volume for everything.

	//This is a list of other food objects to destroy when this food object is destroyed.
	//This is how eating one piece of food off of a plate of things with multiple objects
	//deletes all of the objects at once. Each object has a reference to all the other objects.
	public List<GameObject> thingsToDestroy;	
	GameObject firstController;

	void Start()
	{
		firstController = GameObject.Find("FirstController");	
	}

	void Update()
	{
		isFork = firstController.GetComponent<HandForkBehavior>().isFork;
	}

	void OnDestroy()
	{
		foreach(GameObject thing in thingsToDestroy)
		{
			Destroy(thing);
		}
		//Have to manually set the holdingObject fields to false since OnTriggerStay is still called in between
		//when Destroy is called on this object and when it's actually destroyed, so this is the place to set the
		//fields to false where it should guarantee that OnTriggerStay isn't called anymore in the controller scripts
		//Otherwise, holdingObject stayed true after food got eaten, then the first time the controller touched another food
		//the food would parent to the controller then deparent, and tossObject would be called and the food would start rolling away
		
		if(firstController != null) firstController.GetComponent<HandForkBehavior>().holdingObject = false;
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "GameController")
		{
			if (firstController.GetComponent<ControllerTracker>().id == "Oculus Remote")
			{
				firstController.GetComponent<HandForkBehavior>().heldFood = this.gameObject; // keep track of what we currently have selected

				clearSelection();
				if (isFork)
				{
					highlight_fork.SetActive(true);
				}
				else
				{
					highlight_finger.SetActive(true);
				}
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == "GameController")
		{
			if (firstController.GetComponent<ControllerTracker>().id == "Oculus Remote")
			{
				firstController.GetComponent<HandForkBehavior>().heldFood = null;

				clearSelection();
			}
		}
	}

	public void clearSelection()
	{
		highlight_finger.SetActive(false);
		highlight_fork.SetActive(false);
	}
}
