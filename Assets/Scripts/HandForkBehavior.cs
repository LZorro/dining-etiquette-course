﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class HandForkBehavior : MonoBehaviour {

	/*
		Separate from the ControllerTrackerScript, this script handels the behavior of the controller
		in this game specifically. In general, this class has the code that lets the controller pick
		objects up and the code for changing the controller avatar between a fork and fingers during
		the fork and fingers scene.
		-Logan 9/1/16

	*/




	public ControllerTracker controllerTracker;
	//these values are unnecessary. I only have them in here to quickly and easily test some things
	private float triggerThreshold; //the threshold for when the trigger should no longer be counted as being pressed
	private float trackpadButtonValue;
    private float triggerButtonValue;
    private float gripButtonValue;
    private float menuButtonValue;
    private string controllerID;
    private Vector3 linearVelocity = new Vector3();
    private Vector3 angularVelocity = new Vector3();
	public GameObject fork;	//reference to the fork object that the controller switches into
	public GameObject hand;
    public bool holdingObject;	//keeping track of this prevents multiple object from being picked up at the same time
    public GameObject heldFood;
    public bool isFork;
    private string sceneName;	//stores the scene name so that repeated calls to GetActiveScene aren't necessary


	//fork or fingers scene stuff
	private GameObject menu;

	public Material defaultSkybox;
	public Material restaurantSkybox;
	public Material spaceSkybox;
	public Material officeSkybox;
    

    


	void Start () {
		//set the initial values of some stuff
		holdingObject = false;
		changeControllerTo("hand");

		triggerThreshold = controllerTracker.triggerThreshold;
		controllerID = controllerTracker.id;

		//add a delegate to the sceneLoaded event
		SceneManager.sceneLoaded += onSceneLoad;

		//save the current scene name
		sceneName = SceneManager.GetActiveScene().name;
		ControllerTracker.onGripButtonPressed += changeSkyboxToRestaurant;
		defaultSkybox = RenderSettings.skybox;

	}

	void changeSkyboxToDefault()
	{
		RenderSettings.skybox = defaultSkybox;
		ControllerTracker.onGripButtonPressed -= changeSkyboxToDefault;
		ControllerTracker.onGripButtonPressed += changeSkyboxToRestaurant;
	}

	void changeSkyboxToRestaurant()
	{
		RenderSettings.skybox = restaurantSkybox;
		ControllerTracker.onGripButtonPressed -= changeSkyboxToRestaurant;
		ControllerTracker.onGripButtonPressed += changeSkyboxToSpace;
	}

	void changeSkyboxToSpace()
	{
		RenderSettings.skybox = spaceSkybox;
		ControllerTracker.onGripButtonPressed -= changeSkyboxToSpace;
		ControllerTracker.onGripButtonPressed += changeSkyboxToOffice;
	}

	void changeSkyboxToOffice()
	{
		RenderSettings.skybox = officeSkybox;
		ControllerTracker.onGripButtonPressed -= changeSkyboxToOffice;
		ControllerTracker.onGripButtonPressed += changeSkyboxToDefault;
	}
	


	
	//delegate to be added to the sceneLoaded list so that it gets called whenever a new scene is loaded
	//This makes sure it always starts out in the hand state, so that if you change scenes while being
	//a fork it doesn't stay a fork
	void onSceneLoad(UnityEngine.SceneManagement.Scene scene, UnityEngine.SceneManagement.LoadSceneMode loadMode)
	{
		holdingObject = false;
		changeControllerTo("hand");
		sceneName = SceneManager.GetActiveScene().name;

		RenderSettings.skybox = restaurantSkybox;  // default to restaurant

		//preemptively unsubscribe all scene-specific functions from the button events since the ones
		//for the current scene will be subscribed in the if statements below anyways
		ControllerTracker.onTrackpadButtonPressed -= handForkSwitch;
		ControllerTracker.onMenuButtonPressed -= openMenu;
		BellBehavior.onBellRung -= openMenu;
		if (controllerID == "Oculus Remote")
			ControllerTracker.onTriggerPressed -= remoteClick;

		if(sceneName == "Fork or Fingers")
		{
			ControllerTracker.onTrackpadButtonPressed += handForkSwitch;
			ControllerTracker.onMenuButtonPressed += openMenu;
			BellBehavior.onBellRung += openMenu;
			//get a reference to the menu in the FoF scene, then immediately hide it
			menu = GameObject.Find("Menu");
			menu.SetActive(false);
			if (controllerID == "Oculus Remote");
				ControllerTracker.onTriggerPressed += remoteClick;
		}
		if(sceneName == "Credits")
		{
			ControllerTracker.onMenuButtonPressed += openMenu;
			menu = GameObject.Find("Menu");
			menu.SetActive(false);
		}
	}
	

	void Update () 
	{
		triggerButtonValue = controllerTracker.triggerButtonValue;

		// I'm not sure if the delegate function assignment works, so I'm putting this here as a backup
		// - TJV 10/28/16
		if (controllerID == "Oculus Remote" && triggerButtonValue > triggerThreshold)
			remoteClick();
	}

	//function that opens/closes the menu in the Fork or Fingers scene
	//only gets subscribed when in the Fork or Fingers scene
	void openMenu()
	{
		if(triggerButtonValue > triggerThreshold) return;	//can't open menu while holding something
		// changeControllerTo("hand");	//don't let the user use a fork when the menu is up
		menu.SetActive(!menu.activeInHierarchy);
		StartCoroutine(delayMenuReinteraction(1));
		
	}

	//This function makes it so that there's a one second delay after the bell is rung before it
	//can be rung again. This was necessary because the hand has individual colliders for each 
	//finger, so ringing the bell sometimes resulted in multiple "rings" being registerd at once,
	//so this way when multiple colliders from the hand hit the bell it'll still only respond as intended.
	IEnumerator delayMenuReinteraction(int seconds)
	{
		BellBehavior.onBellRung -= openMenu;
		yield return new WaitForSeconds(seconds); 
		BellBehavior.onBellRung += openMenu;
	}
	
	//Function for switching between the hand and fork. Performs checks to make sure nothing
	//is going on that should prevent the switch, like holding something or if a menu is open
	//subscribe this only when sceneName is "Fork or Fingers"
	void handForkSwitch()
	{
		if(triggerButtonValue > triggerThreshold) return;
		if(isFork) changeControllerTo("hand");
		else changeControllerTo("fork");
	}


	//function for changing what the controller is - right now either a hand or a fork
	// unless we're using Oculus, then always turn the hand/fork off
	void changeControllerTo(string newState)
	{
		fork.SetActive(false);
		hand.SetActive(false);
		switch(newState)
		{
			case "hand":
				//fork.SetActive(false);
				// this.GetComponent<CapsuleCollider>().enabled = true;
				// this.GetComponent<MeshRenderer>().enabled = true;
				if (controllerID != "Oculus Remote")
					hand.SetActive(true);
				isFork = false;
				break;
			case "fork":
				if (controllerID != "Oculus Remote")
					fork.SetActive(true);
				// this.GetComponent<CapsuleCollider>().enabled = false;
				// this.GetComponent<MeshRenderer>().enabled =false;
				//hand.SetActive(false);
				isFork = true;
				break;
		}
	}

	/*
		Function to call ConsumeFood funcitonality upon clicking Main Button trigger when using Oculus Remote 
	*/
	void remoteClick()
	{
		if (heldFood) 
			GameObject.Find("Mouth").GetComponent<ConsumeFood>().eatFood(heldFood);
	}

	//Logic for picking up objects. Parents the transform of the picked up object to the controller.
	//Uses the bool holdingObject to make sure only one object at a time is ever picked up.
	void OnTriggerStay(Collider other)
	{
		if(!other.attachedRigidbody) return;
		// Debug.Log("Picked up object: " + other.attachedRigidbody.gameObject.name + System.Environment.NewLine + "Holding object: " + holdingObject);
		// GameObject pickedUpFood = other.
		if(other.attachedRigidbody.tag.Contains("Food") && !menu.activeInHierarchy)
		{
			if(triggerButtonValue > triggerThreshold && !holdingObject)
			{
				holdingObject = true;
				other.attachedRigidbody.isKinematic = true;
				other.attachedRigidbody.gameObject.transform.SetParent (this.gameObject.transform);
			} else if(triggerButtonValue < triggerThreshold && holdingObject)
			{
				//we know that, in this case, the object is still attached because OnTriggerStay wouldn't be called otherwise
				other.attachedRigidbody.gameObject.transform.SetParent (null);
				other.attachedRigidbody.isKinematic = false;
				holdingObject = false;
				tossObject(other.attachedRigidbody);

			}
			
		}
	}

	//function to pull values from ControllerTracker script, like button values, velocities, and triggerThreshold
	//this script doesn't actually need to get all of these values from the ControllerTracker script, I just have it 
	//do that for convenience to quickly test somethings sometimes. 
	//for performances purposes, this function really shouldn't be called, so in a release version this shouldn't be called
	void getVariousValues()
	{
		trackpadButtonValue = controllerTracker.trackpadButtonValue;
		triggerButtonValue = controllerTracker.triggerButtonValue;
		gripButtonValue = controllerTracker.gripButtonValue;
		menuButtonValue = controllerTracker.menuButtonValue;
		// linearVelocity = controllerTracker.controllerVelocity;
		// angularVelocity = controllerTracker.controllerAngularVelocity;
	}

	void onDestroy()
	{
		//unsubscribe from all events
		SceneManager.sceneLoaded -= onSceneLoad;
		ControllerTracker.onMenuButtonPressed -= openMenu;
		ControllerTracker.onTrackpadButtonPressed -= handForkSwitch;
		BellBehavior.onBellRung -= openMenu;
	}


	//Called when an object is released from being held by the controller, naively adds controller's
	//velocity and angular velocity to it. Better would be to use vector transform for a more realistic
	//throw when throwing something attached to a longer lever arm, but that's low priority currently
	void tossObject(Rigidbody rigidBody)
	{
		rigidBody.velocity = controllerTracker.controllerVelocity;
		rigidBody.angularVelocity = controllerTracker.controllerAngularVelocity;

		//code for doing the vector transform, doesn't quite work, haven't spent time looking into why though
		// Transform origin = this.transform;
		// if (origin != null) {
		// 	rigidBody.velocity = origin.TransformVector (linearVelocity);
		// 	rigidBody.angularVelocity = origin.TransformVector (angularVelocity);
		// } else {
		// 	rigidBody.velocity = linearVelocity;
		// 	rigidBody.angularVelocity = angularVelocity;
		// }
	}
}
