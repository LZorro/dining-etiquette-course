 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript">
      window.API = {
        LMSCommit: () => {console.log("Called commit")}, 
        LMSSetValue: () => {console.log("Called LMSSetValue")},
        LMSInitialize: () => {console.log("LMSInitialize called")}};
      window.finishCourse = function() {
        API.LMSSetValue("cmi.core.lesson_status", "completed");
        API.LMSCommit('');
        console.log("Course finished.");
      };
      $(function () {
        var hasAccess = function (obj) {
          if (obj) {
            try {
              // Attempt to produce a security error.
              if ('location' in obj) {
                obj.location.toString();
              }
            }
            catch (e) {
              return false;
            }
          }
          return true;
        };
        
        var findAPI = function callee(win) {
          if (win && hasAccess(win) && win.API) {
            return win.API;
          }
          return win.parent === win ? null : callee(win.parent);
        };
        window.API = findAPI(window.parent) || findAPI(window.opener) || window.API;

        if(window.API) {
          API.LMSInitialize('');
          API.LMSSetValue('cmi.core.lesson_status', 'incomplete');
          API.LMSCommit('');
        }
      });
    </script>