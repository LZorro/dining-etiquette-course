/* global SendMessage, THREE */
(function () {
  var btnFsEnter = document.querySelector('#btnFsEnter');
  var btnVrToggle = document.querySelector('#btnVrToggle');
  var btnVrReset = document.querySelector('#btnVrReset');
  var canvas = document.querySelector('#canvas');
  var eyeParamsL;
  var eyeParamsR;
  var fullscreen = new Fullscreen();
  var isSupported = 'getVRDisplays' in navigator;
  var frameReady = false;
  var vrFrameData = window.VRFrameData ? new VRFrameData() : null;
  var vrDisplay;
  var vrPose;

  var vrGamepads = [];
  
  //an array of strings that are the names of the unity objects representing the controllers
  //this is used so that calls to SendMessage can be fed controllers[i] as a first argument instead of
  //string literals of the controller names. That means that a dedicated function does't have to be 
  //made for each controller that has the string literals of the controller names hard coded into 
  //each SendMessage call.
  var controllers = [];
  controllers.push('FirstController');
  controllers.push('SecondController');

  if (isSupported) {
    document.body.dataset.supportsVr = 'true';
    document.body.dataset.supportsVrChromium = 'chrome' in window && 'getVRDisplays' in navigator;
  }

  btnFsEnter.addEventListener('click', btnFsEnterOnClick);
  btnVrToggle.addEventListener('click', btnVrToggleOnClick);
  btnVrReset.addEventListener('click', btnVrResetOnClick);

  function btnFsEnterOnClick () {
    // FYI: Unity's `SetFullscreen` doesn't call the unprefixed Fullscreen API.
    fullscreen.enter(canvas);
  }

  function toggleFs () {
    if (fullscreen.isPresenting()) {
      fullscreen.enter(canvas);
    } else {
      fullscreen.exit();
    }
  }

  function btnVrToggleOnClick () {
    btnVrToggle.blur();
    if (vrDisplay) {
      togglePresent();
    } else {
      console.warn('[vrToggle] No VR device was detected');
    }
  }

  function btnVrResetOnClick () {
    btnVrReset.blur();
    if (vrDisplay) {
      resetPose();
    } else {
      console.warn('[btnVrResetOnClick] No VR device was detected');
    }
  }

  function shouldCaptureKeyEvent (e) {
    if (e.shiftKey || e.metaKey || e.altKey || e.ctrlKey) {
      return false;
    }
    return document.activeElement === document.body;
  }

  function initUnityLoaded () {
    document.body.dataset.unityLoaded = 'true';
  }

  function initVrLoaded () {
    if (vrDisplay.capabilities.canPresent) {
      document.body.dataset.vrLoaded = 'true';
    }
  }

  function initFsEventListeners () {
    window.addEventListener('keyup', function (e) {
      if (!shouldCaptureKeyEvent(e)) {
        return;
      }
      if (e.keyCode === 70) {  // `f`.
        if (isSupported) {
          togglePresent();
        } else {
          toggleFs();
        }
      }
    });
  }

  function initVrEventListeners () {
    window.addEventListener('keyup', function (e) {
      if (!shouldCaptureKeyEvent(e)) {
        return;
      }
      if (e.keyCode === 27) {  // `Esc`.
        exitPresent();
      }
      if (e.keyCode === 90) {  // `z`.
        resetPose();
      }
    });
    window.addEventListener('vrdisplaypresentchange', modeChange);
    window.addEventListener('resize', resizeCanvas);
    window.addEventListener('beforeunload', exitPresent);
  }

  function Fullscreen (element) {
    element = element || document.body;
    this.isSupported = true;
    if (element.requestFullscreen) {
      this.element = 'fullscreenElement';
      this.eventChange = 'fullscreenchange';
      this.methodEnter = 'requestFullscreen';
      this.methodExit = 'exitFullscreen';
    } else if (element.mozRequestFullScreen) {
      this.element = 'mozFullScreenElement';
      this.eventChange = 'mozfullscreenchange';
      this.methodEnter = 'mozRequestFullScreen';
      this.methodExit = 'mozCancelFullScreen';
    } else if (element.webkitRequestFullscreen) {
      this.element = 'webkitFullscreenElement';
      this.eventChange = 'webkitfullscreenchange';
      this.methodEnter = 'webkitRequestFullscreen';
      this.methodExit = 'webkitExitFullscreen';
    } else if (element.msRequestFullscreen) {
      this.element = 'msFullscreenElement';
      this.eventChange = 'MSFullscreenChange';
      this.methodEnter = 'msRequestFullscreen';
      this.methodExit = 'msExitFullscreen';
    } else {
      this.isSupported = false;
    }
    this.isPresenting = function () {
      return !!document[this.element];
    }.bind(this);
    this.enter = function (element, options) {
      return element[this.methodEnter](options);
    }.bind(this);
    this.exit = function () {
      return document[this.methodExit]();
    }.bind(this);
  }

  function raf (cb) {
    if (!vrDisplay) {
      return;
    }
    if (vrDisplay.requestAnimationFrame) {
      return vrDisplay.requestAnimationFrame(cb);
    } else {
      return window.requestAnimationFrame(cb);
    }
  }

  function getDisplays () {
    if (navigator.getVRDisplays) {
      isSupported = true;
      return navigator.getVRDisplays().then((devices) => { vrDisplay = devices[0]; });
    } else {
      throw 'Your browser is not VR ready';
    }
  }

  function getEyeParameters () {
    if (!vrDisplay) {
      console.warn('[getEyeParameters] No VR device was detected');
      return;
    }

    if (vrFrameData) {
      SendMessage('WebVRCameraSet', 'eyeL_projectionMatrix', vrFrameData.leftProjectionMatrix.join(','));
      SendMessage('WebVRCameraSet', 'eyeR_projectionMatrix', vrFrameData.rightProjectionMatrix.join(','));
    } else {
      eyeParamsL = vrDisplay.getEyeParameters('left');
      eyeParamsR = vrDisplay.getEyeParameters('right');

      var eyeTranslationL = eyeParamsL.offset[0];
      var eyeTranslationR = eyeParamsR.offset[0];
      var eyeFOVL = eyeParamsL.fieldOfView;
      var eyeFOVR = eyeParamsR.fieldOfView;

      SendMessage('WebVRCameraSet', 'eyeL_translation_x', eyeTranslationL);
      SendMessage('WebVRCameraSet', 'eyeR_translation_x', eyeTranslationR);
      SendMessage('WebVRCameraSet', 'eyeL_fovUpDegrees', eyeFOVL.upDegrees);
      SendMessage('WebVRCameraSet', 'eyeL_fovDownDegrees', eyeFOVL.downDegrees);
      SendMessage('WebVRCameraSet', 'eyeL_fovLeftDegrees', eyeFOVL.leftDegrees);
      SendMessage('WebVRCameraSet', 'eyeL_fovRightDegrees', eyeFOVL.rightDegrees);
      SendMessage('WebVRCameraSet', 'eyeR_fovUpDegrees', eyeFOVR.upDegrees);
      SendMessage('WebVRCameraSet', 'eyeR_fovDownDegrees', eyeFOVR.downDegrees);
      SendMessage('WebVRCameraSet', 'eyeR_fovLeftDegrees', eyeFOVR.leftDegrees);
      SendMessage('WebVRCameraSet', 'eyeR_fovRightDegrees', eyeFOVR.rightDegrees);
    }
  }

  function togglePresent () {
    if (!vrDisplay) {
      return;
    }
    if (isPresenting()) {
      return exitPresent();
    } else {
      return requestPresent();
    }
  }

  function resetPose () {
    if (!vrDisplay) {
      return;
    }
    return vrDisplay.resetPose();
  }

  function getFrameData () {
    if (!vrDisplay) {
      return;
    }
    vrDisplay.getFrameData(vrFrameData);
    return vrFrameData;
  }

  function getPose () {
    if (vrFrameData) {
      return vrFrameData.pose;
    }
    else if (vrDisplay) {
      return vrDisplay.getPose();
    }
  }

  function requestPresent () {
    return vrDisplay.requestPresent([{source: canvas}]);
  }

  function exitPresent () {
    if (!isPresenting()) {
      return;
    }
    return vrDisplay.exitPresent();
  }

  function isPresenting () {
    if (!vrDisplay) {
      return false;
    }
    return vrDisplay.isPresenting;
  }

  function getVRSensorState () {
    getFrameData();
    vrPose = getPose();
    if (!vrPose || vrPose.orientation === null) {
      return;
    }
    var quaternion = new THREE.Quaternion().fromArray(vrPose.orientation);
    var euler = new THREE.Euler().setFromQuaternion(quaternion);
    SendMessage('WebVRCameraSet', 'euler_x', euler.x);
    SendMessage('WebVRCameraSet', 'euler_y', euler.y);
    SendMessage('WebVRCameraSet', 'euler_z', euler.z);
    if (vrPose.position !== null) {
      var positionX = vrPose.position[0];
      var positionY = vrPose.position[1];
      var positionZ = vrPose.position[2];
      if (vrDisplay.displayName.includes("Oculus Rift")) {
        positionY += 1.3;
      }
      SendMessage('WebVRCameraSet', 'position_x', positionX);
      SendMessage('WebVRCameraSet', 'position_y', positionY);
      SendMessage('WebVRCameraSet', 'position_z', positionZ);
    }
  }

    //function is basically a mimic of getVRSensorState, just with a gamepad's pose instead.
  //It accepts the index of a controller in the vrGamepads array as an argument and sends all
  //of the data on that controller (position, rotation, linear and angular velocity, button states)
  //into unity using the SendMessage function. It's assumed that the index of the controller in the
  //vrGamepads is the same as the index of the desired name in the controllers array because it uses 
  //controllers[index] as the argument for the name of the gameObject in unity that the values are being sent to.
  function getGamepadState(index){
    controllerName = controllers[index];
    gamepadPose = vrGamepads[index].pose;

    if (vrGamepads[index].id != "Oculus Remote") {
      if (!gamepadPose || !gamepadPose.orientation || !gamepadPose.position) {
          return;
      }
      var quaternion = new THREE.Quaternion().fromArray(gamepadPose.orientation);
      var euler = new THREE.Euler().setFromQuaternion(quaternion);
        
      SendMessage(controllerName, 'controller_euler_x', euler.x);
      SendMessage(controllerName, 'controller_euler_y', euler.y);
      SendMessage(controllerName, 'controller_euler_z', euler.z);

      var myVelocity = gamepadPose.linearVelocity;
      SendMessage(controllerName, 'getControllerVelocityX', myVelocity[0]);
      SendMessage(controllerName, 'getControllerVelocityY', myVelocity[1]);
      SendMessage(controllerName, 'getControllerVelocityZ', myVelocity[2]);

      var myAngularVelocity = gamepadPose.angularVelocity;
      SendMessage(controllerName, 'getControllerAngularVelocityX', myAngularVelocity[0]);
      SendMessage(controllerName, 'getControllerAngularVelocityY', myAngularVelocity[1]);
      SendMessage(controllerName, 'getControllerAngularVelocityZ', myAngularVelocity[2]);
        
      if (gamepadPose.position !== null) {
        var positionX = gamepadPose.position[0];
        var positionY = gamepadPose.position[1];
        var positionZ = gamepadPose.position[2];
        if (vrDisplay.displayName.includes("Oculus Rift")) {
          positionY += 1.3;
        }
        SendMessage(controllerName, 'getControllerX', positionX);
        SendMessage(controllerName, 'getControllerY', positionY);
        SendMessage(controllerName, 'getControllerZ', positionZ);
      }
    }
    SendMessage(controllerName, 'getControllerID', vrGamepads[index].id);

    checkGamepadButtons(index);

  }

  function resizeCanvas () {
    if (isPresenting()) {
      // TODO: Find a way to get this in the 1.1 API
      if (vrFrameData && vrDisplay.displayName == 'Oculus Rift CV1, Oculus VR') {
        canvas.width = 1080 * 2;
        canvas.height = 1200;
      }
      else {
        eyeParamsL = vrDisplay.getEyeParameters('left');
        eyeParamsR = vrDisplay.getEyeParameters('right');
        canvas.width = Math.max(eyeParamsL.renderWidth, eyeParamsR.renderWidth) * 2;
        canvas.height = Math.max(eyeParamsL.renderHeight, eyeParamsR.renderHeight);
      }
      // TODO: Figure out how to properly mirror the canvas stereoscopically with the v1.0 API in Chromium:
      // https://github.com/gtk2k/Unity-WebVR-Sample-Assets/pull/15
      // See https://github.com/toji/webvr-samples/blob/633a43e/04-simple-mirroring.html#L227-L231
    } else {
      revertCanvas();
    }
  }

  function revertCanvas () {
    canvas.width = document.body.dataset.unityWidth;
    canvas.height = document.body.dataset.unityHeight;
  }

  function modeChange (e) {
    if (isPresenting()) {
      SendMessage('WebVRCameraSet', 'changeMode', 'vr');
      document.body.dataset.vrPresenting = 'true';
      btnVrToggle.textContent = btnVrToggle.title = btnVrToggle.dataset.exitVrTitle;
    } else {
      SendMessage('WebVRCameraSet', 'changeMode', 'normal');
      document.body.dataset.vrPresenting = 'false';
      btnVrToggle.textContent = btnVrToggle.title = btnVrToggle.dataset.enterVrTitle;
    }
    resizeCanvas();
  }

  // Post-render callback from Unity.
  window.postRender = function () {
    if (isPresenting()) {
      frameReady = true;
    }
  };

   //this function is only called once during the vrInit function, so all controllers must 
  //be connected before the page is loaded in order to register the controllers
  function getGamepads(){
    var gamepads = navigator.getGamepads();
    // console.log(gamepads.length);
    for(var i = 0; i < gamepads.length; i++){
      var gamepad = gamepads[i];
      //put any gamepad that's not null and has a pose to it (means it's a vr controller) into the vrGamepads array
      //Had to check the id = OpenVR Gamepad because it was picking up some other random gamepad that was messing things up
      // selects only the right controller - added since Touch is not ambidexterous (TJV)
      if(vrGamepads.length == 0) {
       if (gamepad && (gamepad.id == "OpenVR Gamepad" || gamepad.id.includes("Oculus Remote") || (gamepad.id.includes("Oculus Touch") && gamepad.hand == "right"))) {
            vrGamepads.push(gamepad); 
            return;
        }     
      }
    }
  }

  window.addEventListener("gamepadconnected", function(event){
    if(vrGamepads.length == 0) {
      if (event.gamepad && (event.gamepad.id == "OpenVR Gamepad" || event.gamepad.id.includes("Oculus Remote") || (event.gamepad.id.includes("Oculus Touch") && event.gamepad.hand == "right"))) {
        vrGamepads.push(event.gamepad);
      }
    }
  });


  //checks whether any button on the gamepad is depressed and uses SendMessage to push the 
  //float value representing how much it is depressed back to the controller
  //BUTTON MAPPING:
  //buttons[0] = gamepad (where the thumb is)
  //buttons[1] = trigger
  //buttons[2] = side bar buttons (both of them)
  //buttons[3] = button above gamepad
  function checkGamepadButtons(index)
  {
    controller = vrGamepads[index];

    if (controller.id == "Oculus Remote")
    {
      SendMessage(controllers[index], 'getTriggerState', controller.buttons[0].value);
      SendMessage(controllers[index], 'getMenuButtonState', controller.buttons[1].value);
      // any D-pad button functions as the TrackpadButton
      SendMessage(controllers[index], 'getTrackpadButtonState', controller.buttons[2].value | controller.buttons[3].value | controller.buttons[4].value | controller.buttons[5].value);
      // SendMessage(controllers[index], 'getGripButtonState', controller.buttons[3].value); // no grip buttons on Remote
    }
    else
    {
      SendMessage(controllers[index], 'getTrackpadButtonState', controller.buttons[0].value);
      SendMessage(controllers[index], 'getTriggerState', controller.buttons[1].value);
      SendMessage(controllers[index], 'getGripButtonState', controller.buttons[2].value);
      SendMessage(controllers[index], 'getMenuButtonState', controller.buttons[3].value);
    }
  }

  // Initialisation callback from Unity (called by `StereoCamera.cs`).
  window.vrInit = function () {
    initUnityLoaded();
    initFsEventListeners();
    if (!isSupported) {
      // Bail early in case browser lacks Promises support (for below).
      console.warn('WebVR is not supported');
      return;
    }
    getGamepads();                    // gamepad functionality not in original UnityWebVRManager
    getDisplays().then(function () {
      initVrLoaded();
      initVrEventListeners();
      getFrameData();
      getEyeParameters();
      resizeCanvas();
      window.requestAnimationFrame(update);
    }).catch(console.error.bind(console));
  };

  var update = function () {
    if (isPresenting() && frameReady) {
      vrDisplay.submitFrame();
      frameReady = false;
    }
    getVRSensorState();
    for(var i = 0; i < vrGamepads.length; i++){
      getGamepadState(i);
    }
    raf(update);
  };
})();
